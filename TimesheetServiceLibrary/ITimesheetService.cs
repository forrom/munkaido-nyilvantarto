﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Timesheet;
using Timesheet.TransferObjects;

namespace TimesheetServiceLibrary
{
    [ServiceContract(SessionMode=SessionMode.Required)]
    public interface ITimesheetService
    {

        #region Users + Auth

        [OperationContract(IsInitiating = true)]
        [FaultContract(typeof(FaultException))]
        void Register(string username, string password);

        [OperationContract(IsInitiating = true)]
        void Login(string username, string password);

        #endregion
        
        #region Project CRUD
        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        ProjectDTO GetProject(int projectID);

        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        ProjectDTO[] GetProjects();

        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        ProjectDTO CreateProject(string name, string description, int leaderId);

        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        ProjectDTO UpdateProjectName(int projectId, string name);


        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        ProjectDTO UpdateProjectDescription(int projectId, string description);

        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        ProjectDTO UpdateProjectLeader(int projectId, int leaderId);

        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        void DeleteProject(int projectId);

        #endregion

        #region Task CRUD
        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        TaskDTO GetTask(int taskID);


        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        TaskDTO CreateTask(string name, string description, int parentProjectId);


        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        void UpdateTaskName(int taskId, string name);

        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        void UpdateTaskDescription(int taskId, string description);

        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        void UpdateTaskProject(int taskId, int parentProjectId);

        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        void DeleteTask(int taskId);

        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        TaskDTO[] GetTasksInProject(int projectId);

        #endregion

        #region Work CRUD

        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        WorkDTO CreateWork(int parentTaskId, int workerId, string description, DateTime startTime, DateTime finishTime);


        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        void UpdateWorkDescription(int workId, string description);


        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        void UpdateWorkTask(int workId, int taskId);


        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        void updateWorkWorker(int workId, int workerId);


        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        void UpdateWorkStartTime(int workId, DateTime startTime);


        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        void UpdateWorkFinishTime(int workId, DateTime finishTime);

        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        void DeleteWork(int workId);

        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        WorkDTO[] GetWorkFromTask(int taskId);

        [OperationContract(IsInitiating = false)]
        [FaultContract(typeof(FaultException))]
        WorkDTO[] GetWorkFromUser(int userId);



        #endregion
    }

}
