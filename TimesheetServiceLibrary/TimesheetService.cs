﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Timesheet;
using Timesheet.TransferObjects;

namespace TimesheetServiceLibrary
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class TimesheetService : ITimesheetService
    {
        const string employee_role = "employee", manager_role="manager";
        string username;
        string[] roles;


        #region Users + Auth
        private User getUserByName(TimesheetDataModelContainer db, string userName)
        {
            var user = from usr in db.Users
                       where usr.Name == userName
                       select usr;
            if (user.First() == null)
            {
                throw new FaultException("User not found");
            }
            return user.First();

        }


        private void checkRole(string role)
        {
            if (!roles.Contains(role, StringComparer.OrdinalIgnoreCase))
            {
                throw new FaultException("Unauthorized: required role is " + role);
            }
        }

        private User getUser(TimesheetDataModelContainer db, int leaderId)
        {
            var user = from usr in db.Users
                       where usr.Id == leaderId
                       select usr;
            return user.First();
        }



        public void Login(string username, string password)
        {
            using (var db = new TimesheetDataModelContainer())
            {
                var query = from u in db.Users where u.Name.Equals(username) select u;
                if (query.First() == null)
                {
                    throw new FaultException();
                }


                string passwordHash = query.First().Password, salt = query.First().Salt;



                if (hashMatches(password, salt, passwordHash))
                {
                    setRoles(db, query.First());
                    this.username = username;
                }
                else
                {
                    throw new FaultException();
                }
            }

        }

        private void setRoles(TimesheetDataModelContainer db, User user)
        {

            var roleNames = from rol in db.Roles
                            where rol.User.Any(ra => ra.Id == user.Id)
                            select rol;
            HashSet<string> roleSet = new HashSet<string>();
            foreach (Role u2 in roleNames)
            {
                roleSet.Add(u2.Name);
            }
            roles = roleSet.ToArray();
        }


        private bool hashMatches(string password, string salt, string passwordHash)
        {
            return Security.Security.ComputeSaltedHash(password, salt).Equals(passwordHash);

        }

        public void Register(string username, string password)
        {
            using (var db = new TimesheetDataModelContainer())
            {

                var usr = from user
                          in db.Users
                          where user.Name.Equals(username)
                          select user;
                if (usr.Count() > 0)
                {
                    throw new FaultException("User already exists");
                }

                string salt = Security.Security.GenerateSaltString(), saltedHash = Security.Security.ComputeSaltedHash(password, salt); ;

                User u = new User();
                u.Name = username;
                u.Password = saltedHash;
                u.Salt = salt;

                db.Users.Add(u);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }


        #endregion

        #region Project CRUD
        public ProjectDTO CreateProject(string name, string description, int leaderId)
        {
            checkRole(manager_role);

            Project project = new Project();

            using (var db = new TimesheetDataModelContainer())
            {
                User leader = getUser(db, leaderId);

                project.Name = name;
                project.Leader = leader;
                project.Description = description;

                db.Projects.Add(project);
                db.SaveChanges();
            }

            return new ProjectDTO(project);

        }

        public void DeleteProject(int projectId)
        {
            checkRole(manager_role);
            using (var db = new TimesheetDataModelContainer())
            {
                Project project = getProjectById(projectId, db);
                db.Projects.Remove(project);
                db.SaveChanges();
            }
        }


        public ProjectDTO[] GetProjects()
        {

            checkRole(manager_role);

            var projects = new HashSet<ProjectDTO>();

            int i = 0;
            using (var db = new TimesheetDataModelContainer())
            {
                foreach (Project project in db.Projects)
                {
                    projects.Add(new ProjectDTO(project));
                    i++;
                }
            }
            return projects.ToArray();
        }

        public ProjectDTO GetProject(int projectID)
        {
            checkRole(employee_role);
            using (var db = new TimesheetDataModelContainer())
            {
                return new ProjectDTO(getProjectById(projectID, db));
            }

        }

        Project getProjectById(int projectID, TimesheetDataModelContainer db)
        {
            Project project = db.Projects.Where(p => p.Id == projectID).First();
            return project;

        }
        public ProjectDTO UpdateProjectDescription(int projectId, string description)
        {
            checkRole(manager_role);

            using (var db = new TimesheetDataModelContainer())
            {
                Project project = getProjectById(projectId, db);
                project.Description = description;

                db.SaveChanges();

                return new ProjectDTO(project);
            }
        }

        public ProjectDTO UpdateProjectLeader(int projectId, int leaderId)
        {
            checkRole(manager_role);

            using (var db = new TimesheetDataModelContainer())
            {
                Project project = getProjectById(projectId, db);
                project.UserId = getUser(db, leaderId).Id;

                db.SaveChanges();

                return new ProjectDTO(project);
            }
        }
        
        public ProjectDTO UpdateProjectName(int projectId, string name)
        {
            checkRole(manager_role);

            using (var db = new TimesheetDataModelContainer())
            {
                Project project = getProjectById(projectId, db);
                project.Name = name;

                db.SaveChanges();

                return new ProjectDTO(project);
            }
        }

        #endregion

        #region Task CRUD

        public TaskDTO GetTask(int taskID)
        {
            using (var db = new TimesheetDataModelContainer())
            {
                Task task = db.Tasks.Where(t => t.Id == taskID).First();
                return new TaskDTO(task);
            }
        }

        public TaskDTO CreateTask(string name, string description, int parentProjectId)
        {
            using (var db = new TimesheetDataModelContainer())
            {

                Task task = new Task();
                task.Name = name;
                task.Description = description;
                task.Project = db.Projects.Where(p => p.Id == parentProjectId).First();

                db.Tasks.Add(task);

                db.SaveChanges();
                return new TaskDTO(task);
            }
        }

        private Task getTaskById(int taskId, TimesheetDataModelContainer db)
        {
            var task = db.Tasks.Where(t => t.Id == taskId);
            return task.First();
        }
        public void UpdateTaskName(int taskId, string name)
        {

            using (var db = new TimesheetDataModelContainer())
            {
                Task task = db.Tasks.Where(t => t.Id == taskId).First();
                task.Name = name;
                db.SaveChanges();
            }
        }

        public void UpdateTaskDescription(int taskId, string description)
        {

            using (var db = new TimesheetDataModelContainer())
            {
                Task task = db.Tasks.Where(t => t.Id == taskId).First();
                task.Description = description;
                db.SaveChanges();
            }
        }

        public void UpdateTaskProject(int taskId, int parentProjectId)
        {
            using (var db = new TimesheetDataModelContainer())
            {
                Task task = db.Tasks.Where(t => t.Id==taskId).First();
                task.Project = db.Projects.Where(p => p.Id == parentProjectId).First();

                db.SaveChanges();
            }
        }

        public void DeleteTask(int taskId)
        {
            using (var db = new TimesheetDataModelContainer())
            {
                Task task = db.Tasks.Where(t => t.Id == taskId).First();
                db.Tasks.Remove(task);
                db.SaveChanges();
            }
        }

        public TaskDTO[] GetTasksInProject(int projectId)
        {
            using (var db = new TimesheetDataModelContainer())
            {
                var query = db.Tasks.Where(t => t.ProjectId == projectId);

                List<TaskDTO> resultList = new List<TaskDTO>();

                query.ToList().ForEach(t => resultList.Add(new TaskDTO(t)));
                return resultList.ToArray();
            }
        }
        #endregion

        #region Work CRUD
        
        public WorkDTO CreateWork(int parentTaskId, int workerId, string description, DateTime startTime, DateTime finishTime)
        {
            
            using(var db = new TimesheetDataModelContainer())
            {
                Work work = new Work();
                Task parentTask = db.Tasks.Where(t => t.Id == parentTaskId).First();
                User worker = db.Users.Where(u => u.Id == workerId).First();

                work.Task = parentTask;
                work.Worker = worker;
                work.Description = description;
                work.StartDate = startTime;
                work.FinishDate = finishTime;

                db.WorkSet.Add(work);
                db.SaveChanges();

                return new WorkDTO(work);
            }
        }

        public void UpdateWorkDescription(int workId, string description)
        {
            using (var db = new TimesheetDataModelContainer())
            {
                Work work = db.WorkSet.Where(w => w.Id == workId).First();
                work.Description = description;
                db.SaveChanges();
            }
        }

        public void UpdateWorkTask(int workId, int taskId)
        {
            using (var db = new TimesheetDataModelContainer())
            {
                Work work = db.WorkSet.Where(w => w.Id == workId).First();
                work.Task = db.Tasks.Where(t => t.Id == taskId).First();
                db.SaveChanges();
            }
        }

        public void updateWorkWorker(int workId, int workerId)
        {
            using (var db = new TimesheetDataModelContainer())
            {
                Work work = db.WorkSet.Where(w => w.Id == workId).First();
                work.Worker = db.Users.Where(u => u.Id == workerId).First();
                db.SaveChanges();
            }
        }

        public void UpdateWorkStartTime(int workId, DateTime startTime)
        {
            using (var db = new TimesheetDataModelContainer())
            {
                Work work = db.WorkSet.Where(w => w.Id == workId).First();
                work.StartDate = startTime;
                db.SaveChanges();
            }
        }

        public void UpdateWorkFinishTime(int workId, DateTime finishTime)
        {
            using (var db = new TimesheetDataModelContainer())
            {
                Work work = db.WorkSet.Where(w => w.Id == workId).First();
                work.FinishDate = finishTime;
                db.SaveChanges();
            }
        }

        public void DeleteWork(int workId)
        {
            using (var db = new TimesheetDataModelContainer())
            {
                Work work = db.WorkSet.Where(w => w.Id == workId).First();
                db.WorkSet.Remove(work);
                db.SaveChanges();
            }
        }

        public WorkDTO[] GetWorkFromTask(int taskId)
        {
            using (var db = new TimesheetDataModelContainer())
            {
                var query = db.WorkSet.Where(w => w.TaskId== taskId);

                List<WorkDTO> resultList = new List<WorkDTO>();

                query.ToList().ForEach(w => resultList.Add(new WorkDTO(w)));
                return resultList.ToArray();
            }
        }

        public WorkDTO[] GetWorkFromUser(int userId)
        {
            using (var db = new TimesheetDataModelContainer())
            {
                var query = db.WorkSet.Where(w => w.UserId== userId);

                List<WorkDTO> resultList = new List<WorkDTO>();

                query.ToList().ForEach(w => resultList.Add(new WorkDTO(w)));
                return resultList.ToArray();
            }
        }
        #endregion
    }
    
}
