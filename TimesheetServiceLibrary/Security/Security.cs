﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace TimesheetServiceLibrary.Security
{
    public class Security
    {


        public static string ComputeSaltedHash(string password, string salt)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] source = ASCIIEncoding.ASCII.GetBytes(password), target, saltBytes=ASCIIEncoding.ASCII.GetBytes(salt);
            byte[] saltySource = AddByteArrays(source, saltBytes);
            target = md5.ComputeHash(saltySource);
            Console.WriteLine(ToHex(target));
            return ToHex(target);
        }
        public static string GenerateSaltString()
        {
            byte[] target = new byte[16];
            var rng = new RNGCryptoServiceProvider();

            rng.GetNonZeroBytes(target);

            return ToHex(target);
        }

        private static string ToHex(byte[] bytes)
        {
            StringBuilder result = new StringBuilder(bytes.Length * 2);

            for (int i = 0; i < bytes.Length; i++)
                result.Append(bytes[i].ToString("x2"));

            return result.ToString();
        }

        private static byte[] AddByteArrays(byte[] arr1, byte[] arr2)
        {
            int n = Math.Max(arr1.Length, arr2.Length);
            byte[] result = new byte[n];
            for(int i=0; i<n; i++)
            {
                if (i >= arr1.Length)
                {
                    result[i] = arr2[i];
                }
                else if (i >= arr2.Length)
                {
                    result[i] = arr1[i];
                }
                else
                {
                    result[i] = (byte)(arr1[i] + arr2[i]);
                }
            }
            return result;
        }
    }
}
