﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Windows.Forms;
using Timesheet.TransferObjects;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        ServiceReference1.TimesheetServiceClient cl = new ServiceReference1.TimesheetServiceClient(); 
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cl = new ServiceReference1.TimesheetServiceClient();
            cl.Login("Pisti", "asdf");
            MessageBox.Show("Login successful");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            cl.UpdateTaskDescription(1, "Hello");
            MessageBox.Show("Done");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            WorkDTO[] work = cl.GetWorkFromTask(1);
            foreach(WorkDTO w in work)
            {
                MessageBox.Show(w.Description);
            }
        }
    }
}
