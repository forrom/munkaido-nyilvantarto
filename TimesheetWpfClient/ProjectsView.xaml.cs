﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TimesheetWpfClient.ServiceReference1;

namespace TimesheetWpfClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ProjectsView : Window
    {
        ServiceReference1.TimesheetServiceClient cl;
        public ProjectsView()
        {
            cl = ServiceClientProvider.GetInstance();
            cl.Login("Pisti", "asdf");


            DataContext = new ProjectsVM(cl.GetProjects());

            InitializeComponent();
        }

        private void deleteProjectButton_Click(object sender, RoutedEventArgs e)
        {
            ProjectDTO selected = (ProjectDTO)Projects.SelectedValue;
            cl.DeleteProject(selected.Id);

            DataContext = new ProjectsVM(cl.GetProjects());
        }
    }
}
