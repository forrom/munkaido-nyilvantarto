﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimesheetWpfClient.ServiceReference1;

namespace TimesheetWpfClient
{
    class ProjectsVM { 

    private ObservableCollection<ProjectDTO> projects = new ObservableCollection<ProjectDTO>(); 
  

    public ObservableCollection<ProjectDTO> Projects
    {
        get
        {
            return projects;
        }
    }
        internal ProjectsVM(ProjectDTO[] projects)
        {
        this.projects = new ObservableCollection<ProjectDTO>(projects.ToList());
        }
    }
}
