﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimesheetWpfClient
{
    class ServiceClientProvider
    {
        static ServiceReference1.TimesheetServiceClient instance;

        public static ServiceReference1.TimesheetServiceClient GetInstance()
        {
            if (instance == null)
            {
                instance = new ServiceReference1.TimesheetServiceClient();
            }
            return instance;
        }
        
    }
}
