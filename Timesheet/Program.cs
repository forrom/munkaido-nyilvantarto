﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheet
{
    class Program
    {
        static void Main(string[] args)
        {
            using(var db = new TimesheetDataModelContainer())
            {
                /*
                User u = new User();
                u.Name = "Jóska";
                u.Password = "asdf";

                Role r = new Role();
                r.Name = "Employee";


                Role r2 = new Role();
                r2.Name = "Manager";
                
                u.Role.Add(r);
                u.Role.Add(r2);

                db.Users.Add(u);
                db.Roles.Add(r);
                db.Roles.Add(r2);
                */

                User u = new User();
                u.Name = "Karcsi";
                u.Password = "asdf";

                var r = from role in db.Roles where role.Name=="Employee" select role;
                u.Role.Add(r.First());
                               
                db.Users.Add(u);
                db.SaveChanges();
            }
        }
    }
}
