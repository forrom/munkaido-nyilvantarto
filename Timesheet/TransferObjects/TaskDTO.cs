﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheet.TransferObjects
{
    [Serializable]
    public class TaskDTO
    {
        public readonly string Name;
        public readonly string Description;
        public readonly int Id, ProjectID;

        public TaskDTO(Task task)
        {
            Name = task.Name;
            Description = task.Description;
            ProjectID = task.Project.Id;
            Id = task.Id;
        }
    }
}

