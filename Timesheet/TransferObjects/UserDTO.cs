﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheet.TransferObjects
{
    [Serializable]
    public class UserDTO
    {
        public readonly string Name;
        public readonly int Id;

        public UserDTO(User user)
        {
            Id = user.Id;
            Name = user.Name;


        }
    }
}
