﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheet.TransferObjects
{
    [Serializable]
    public class RoleDTO
    {
        public readonly int Id;
        public readonly string Name;

        public RoleDTO(Role role)
        {
            Id = role.Id;
            Name = role.Name;
        }
    }
}
