﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheet.TransferObjects
{
    [Serializable]
    public class ProjectDTO
    {
        public readonly string Name;
        public readonly string Description;
        public readonly int Id, LeaderID;
        
        public ProjectDTO(Project project)
        {
            Name = project.Name;
            Description = project.Description;
            Id = project.Id;
            if (project.Leader != null)
            {
                LeaderID = project.Leader.Id;
            }
        }
    }
}
