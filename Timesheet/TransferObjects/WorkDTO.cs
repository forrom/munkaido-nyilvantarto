﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheet.TransferObjects
{
    [Serializable]
    public class WorkDTO
    {
        public readonly string Name;
        public readonly string Description;
        public readonly int Id, TaskID, WorkerID;
        public readonly DateTime StartDate, FinishDate;

        public WorkDTO(Work work)
        {
            Id = work.Id;
            TaskID = work.Task.Id;
            Description = work.Description;
            WorkerID = work.Worker.Id;
            StartDate = new DateTime(work.StartDate.Ticks);
            if (work.FinishDate.HasValue) { 
                FinishDate = new DateTime(work.FinishDate.GetValueOrDefault().Ticks);
            }
        }
    }
}
