﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoneApp1.ServiceReference1;

namespace PhoneApp1
{
    class PhoneProjects
    {
        private ObservableCollection<ProjectDTO> projects = new ObservableCollection<ProjectDTO>();


        public ObservableCollection<ProjectDTO> Projects
        {
            get
            {
                return projects;
            }
        }
        internal PhoneProjects(ProjectDTO[] projects)
        {
            this.projects = new ObservableCollection<ProjectDTO>(projects.ToList());
        }
    }

}

