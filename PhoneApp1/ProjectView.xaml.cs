﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PhoneApp1.ServiceReference2;

namespace PhoneApp1
{
    public partial class ProjectView : PhoneApplicationPage
    {
        private TimesheetServiceClient client;
        private ProjectDTO project;

        public ProjectView(TimesheetServiceClient client, ObservableCollection<TaskDTO> tasks)
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            this.project = (ProjectDTO) PhoneApplicationService.Current.State["selectedProject"];
            this.client = (TimesheetServiceClient)PhoneApplicationService.Current.State["Client"];
            DataContext = project;
            client.GetTasksInProjectCompleted += client_GetTasksInProjectCompleted;
            client.GetTasksInProjectAsync(project.Id);
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            client.GetTasksInProjectCompleted += client_GetTasksInProjectCompleted;
            client.GetTasksInProjectAsync(project.Id);
        }

        private void client_GetTasksInProjectCompleted(object sender, GetTasksInProjectCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                Tasks.ItemsSource = e.Result;
            }
        }

        private void ViewButton_Click(object sender, RoutedEventArgs e)
        {
            TaskDTO selectedTask = (TaskDTO)Tasks.SelectedItem;
            PhoneApplicationService.Current.State["selectedTask"] = selectedTask;
            PhoneApplicationService.Current.State["Client"] = client;
            NavigationService.Navigate(new Uri("/TaskView.xaml", UriKind.Relative));
        }
    }
}