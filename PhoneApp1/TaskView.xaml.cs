﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PhoneApp1.ServiceReference2;
using System.Collections.ObjectModel;

namespace PhoneApp1
{
    public partial class TaskView : PhoneApplicationPage
    {
        TaskDTO task;
        ObservableCollection<WorkDTO> works;
        TimesheetServiceClient client;

        public TaskView()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            this.task = (TaskDTO)PhoneApplicationService.Current.State["selectedTask"];
            this.client = (TimesheetServiceClient)PhoneApplicationService.Current.State["Client"];
            client.GetWorkFromTaskCompleted += client_GetWorkFromTaskCompleted;
            client.GetWorkFromTaskAsync(task.Id);
        }

        private void client_GetWorkFromTaskCompleted(object sender, GetWorkFromTaskCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                works = e.Result;
                WorkList.ItemsSource = works;
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ViewButton_Click(object sender, RoutedEventArgs e)
        {

        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}