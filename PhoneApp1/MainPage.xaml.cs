﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PhoneApp1.Resources;
using System.Collections.ObjectModel;
using PhoneApp1.ServiceReference2;

namespace PhoneApp1
{
    public partial class MainPage : PhoneApplicationPage
    {
        TimesheetServiceClient client;
        ObservableCollection<UserDTO> managers;

        void client_GetManagersCompleted(object sender, GetManagersCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                managers = e.Result;
            }
        }

        public MainPage(TimesheetServiceClient client)
        {
            this.client = client;
            client.GetManagersCompleted += client_GetManagersCompleted;
            client.GetManagersAsync();

            InitializeComponent();
            client.GetProjectsCompleted += client_GetProjectsCompleted;
            client.GetProjectsAsync();
        }

        private void client_GetProjectsCompleted(object sender, GetProjectsCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                DataContext = e.Result;
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            client.GetProjectsCompleted += client_GetProjectsCompleted;
            client.GetProjectsAsync();
        }

        private void ViewButton_Click(object sender, RoutedEventArgs e)
        {
            ProjectDTO selected = (ProjectDTO)ProjectSelector.SelectedItem;
            PhoneApplicationService.Current.State["selectedProject"] = selected;
            PhoneApplicationService.Current.State["Client"] = client;
            NavigationService.Navigate(new Uri("/ProjectView.xaml", UriKind.RelativeOrAbsolute));
        }
    }
}