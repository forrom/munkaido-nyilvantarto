﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PhoneApp1.ServiceReference2;

namespace PhoneApp1
{
    public partial class PhoneLogin : PhoneApplicationPage
    {
        TimesheetServiceClient client;

        public PhoneLogin(TimesheetServiceClient client)
        {
            this.client = client;
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                client.LoginCompleted += client_LoginCompleted;
                client.LoginAsync(UserNameBox.Text, PasswordBox.Text);
            }
            catch
            {
                MessageBox.Show("Login failed");
            }
        }

        private void client_LoginCompleted(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                MessageBox.Show("Login Success");
                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            }
        }
    }
}