﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhoneApp1.ServiceReference2;

namespace PhoneApp1
{
    public class Bootstrap
    {
        public Bootstrap Instance = new Bootstrap();
        public ServiceReference2.TimesheetServiceClient client;
        public ProjectDTO selected;

        Bootstrap()
        {
            if (client == null)
            {
                client = new ServiceReference2.TimesheetServiceClient();         
            }
        }

        public PhoneLogin createLoginPage()
        {
            return new PhoneLogin(client);
        }

        public MainPage CreateProjectsView()
        {
            return new MainPage(client);
        }

        public StatementView createStatementView()
        {
            return new StatementView(client);
        }
    }
}
